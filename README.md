# j2paas-templates

#### 一、介绍
J2PaaS Web前端登录和后台管理模板库

#### 二、项目结构

 - css 目录，模板页面样式文件
 - js 目录，关联的js脚本文件
 - img 目录, 关联的图片文件
 - login.jpg 文件，模板页面的最终显示效果图
 - login.zul 文件，关联的模板页面

#### 三、模板分类
  
 - 以login和main作为区分登录模板和管理后台主页模板，例如登录模板:
   

   **login.zul页面代码**

```xml
    <?style type="text/css" href="/template/#{708}/css/style.css"?>
    <div xmlns:n="native" zclass="w-100 h-100">
        <n:div class="bg1"/>
        <n:div class="gyl">
            EP 免编码
            <n:div class="gy2">敏捷开发的先行者；覆盖面最全、高效、安全、稳定的平台</n:div>
        </n:div>
        <n:form>
            <div zclass="bg" id="loginDiv" apply="cn.easyplatform.web.controller.LoginController">
                <n:div class="wel">用户登录</n:div>
                <n:div class="user">
                    <n:div id="yonghu">用户名</n:div>
                    <textbox placeholder="请输入账号" id="userId" zclass="none" value="lxr"/>
                </n:div>
                <n:div class="password">
                    <n:div id="yonghu">密&#160;&#160;&#160;码</n:div>
                    <textbox placeholder="请输入密码" type="password" id="userPassword" zclass="none"
                             value="1"/>
                </n:div>
                <n:div class="rem">
                    <n:div id="yonghu">验证码</n:div>
                    <textbox placeholder="请输入验证码" zclass="none" id="captcha"/>
                    <captcha width="100px" height="30px" onClick="self.randomValue()" fontColor="#007bff"/>
                </n:div>
                <button zclass="btn" id="login">登&#160;&#160;&#160;录</button>
            </div>
        </n:form>
    </div>
```

**效果图**

![Jar_File_summary.jpg](login001/src/main/resources/login.jpg)

### 四、打包安装

执行：

```shell
 > cd 模板项目目录
 > mvn install
```

拷贝模板项目生成的jar文件到J2PaaS部署公共模板目录下，进入管理后台，选择“模板管理"菜单，在登录页面点击“选择模板”按钮就会出现新模板选项项，如图：

<br/>

![1632362120.jpg](.gitee/1632362120.jpg)
